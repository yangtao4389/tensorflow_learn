import tensorflow as tf

c = tf.constant([1, 2, 3])

queue = tf.FIFOQueue(capacity=3, dtypes=tf.int32)

# 塞入多个元素
init_queue = queue.enqueue_many(c)

# 出队+1，再入队
var = queue.dequeue()
var_add = var + 1

# 入队
enqueue_op = queue.enqueue(var_add)

with tf.Session() as sess:
    sess.run(init_queue)
    # 当执行这一句话就会执行涉及到的所有操作
    sess.run(enqueue_op)

    # 此时队列数据为：2 3 2
    for i in range(3):
        print(queue.dequeue().eval())
