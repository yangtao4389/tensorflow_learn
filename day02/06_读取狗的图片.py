import os

import tensorflow as tf

'''
1、构造图片文件队列

2、构造图片阅读器，读取图片数据

4、解码图片，统一图片大小

5、开启会话，启动多线程

6、批处理

'''
# 1、构造图片文件队列
file_path = '/home/python/PycharmProjects/tensorflow_learn/day02/dog'
files = os.listdir(file_path)
# 变为绝对路径列表
file_names = [os.path.join(file_path,file) for file in files]
print(file_names)
# 创建队列
queue = tf.train.string_input_producer(file_names)

# 2、构造图片阅读器，读取图片数据
reader = tf.WholeFileReader()
# key文件名，value文件的内容
key,value = reader.read(queue)

# 4、解码图片，统一图片大小
# 通道数为3
image = tf.image.decode_jpeg(value,channels=3)
# 设置图片大小
image_resize = tf.image.resize_images(image, size=[256,256])

#6 批处理维护一个队列
#  batch_size 读取的个数  threads线程数  capacity 容量
image_batch =tf.train.batch([image_resize],batch_size=10,num_threads=3,capacity=10)
print(image_batch)
# Tensor("batch:0", shape=(10, 256, 256, 3), dtype=float32)


# 5、开启会话，启动多线程
with tf.Session() as sess:
    # 协调器
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess,coord=coord)

    # 看一下图片的取值
    # print(image_resize.eval())

    # 批处理的数据
    print(image_batch.eval())

    coord.request_stop()
    coord.join(threads=threads)







