import os

import tensorflow as tf

'''
1、构建文件名（绝对路径）队列

2、构建文件阅读器，读取内容

3、解码内容，指定默认值或者缺省值

4、开启会话，启动队列管理器

'''
# 1、构建文件名（绝对路径）队列
file_path = '/home/python/PycharmProjects/tensorflow_learn/day02/csvdata'
files = os.listdir(file_path)
print(files)
# 构建绝对路径列表
file_path_list = [os.path.join(file_path, file) for file in files]
print(file_path_list)
# 构建队列
# string_input_producer返回一个队列，并且创建队列对应的QueueRunner,
# 并将这个QueueRunner加入计算图的QueueRunner集合
queue = tf.train.string_input_producer(file_path_list)

# 2、构建文件阅读器，读取内容
reader = tf.TextLineReader()
# 从队列中读取文件,返回key文件名，value默认csv中是一行内容
key, value = reader.read(queue)

# 3、解码内容，指定默认值或者缺省值
# record_defaults为空默认的填充值
# 读取的一行数据有两列，所以返回一个列,两个值
value1, value2 = tf.decode_csv(record_defaults=[['None'], ['None']], records=value)

# 4、开启会话，启动队列管理器
with tf.Session() as sess:
    # 为什么可以直接启动queue_runners
    #   Starts all queue runners collected in the graph.
    # 协调器
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    # ...
    print([value1.eval(),value2.eval()])

    # 请求线程停止
    coord.request_stop()
    coord.join(threads)
