import tensorflow as tf

# 定义一个队列
queue = tf.FIFOQueue(capacity=2,dtypes=tf.int32)


c = tf.constant(1000)
c = tf.constant([10,11,12])

# 定义入队的操作
# 入一个tensor
enqueue_op = queue.enqueue(vals=c)
# 入多个tensor
enqueue_op = queue.enqueue_many(vals=c)

print(enqueue_op)

#出队 返回一个tensor，不属于操作
out = queue.dequeue()
print(out)

# 执行入队
with tf.Session() as sess:
    print("入队之前的队列大小：",queue.size().eval())
    sess.run(enqueue_op)
    # 队列的大小
    print("入队之后的队列大小：",queue.size().eval())

    # 获取出队的tensor值
    print("出队：",out.eval())
    #todo 注意如果出队或者入队，只要没数据，或者大于size，就会一直等待
    #todo 下面的操作就处于等待状态
    print("出队：",out.eval())
    print("出队：",out.eval())



