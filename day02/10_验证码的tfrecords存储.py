import os

import tensorflow as tf

def read_csv():
    # 1 读取标签csv文件内容
    file_path = '/home/python/PycharmProjects/tensorflow_learn/day02/GenPics/labels.csv'
    # shuffle变为False，不能随机出队
    queue = tf.train.string_input_producer([file_path], shuffle=False)

    # 2 csv阅读器
    reader = tf.TextLineReader()
    # 读取的为一行数据
    key,value = reader.read(queue)
    # 0 NEPP

    # 解码 ,填充默认值
    index, label = tf.decode_csv(records=value, record_defaults=[[0],['None']])
    # 返回的label为bytes类型

    # 总共6000条数据，一次性全部读取
    label_batch = tf.train.batch([label], batch_size=6000, num_threads=3, capacity=6000)

    return label_batch



LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
# 将字符串转换为数字的形式 a->0
def transform_label_to_digist(label_batch):
    # [b'nzpp',]

    # 构建一个字典数据类型
    # {A:0,B:1}
    num_letter = dict(enumerate(list(LETTERS)))
    # 键值反转
    letter_num = dict(zip(num_letter.values(),num_letter.keys()))

    result = []
    # 遍历6000个字符串
    for i in range(6000):
        # NZPP对应的数字列表
        label_digits = []
        for letter in label_batch[i].decode():
            # 获取到一个字母的数字，加入到label digits中
            label_digits.append(letter_num[letter])
        result.append(label_digits)
    return result

# 读取验证码图片
def read_captcha():
    '''

    :return: 返回image_batch6000张图片
    '''
    file_path = '/home/python/PycharmProjects/tensorflow_learn/day02/GenPics'
    # os.listdir(file_path)#todo 读取不是按顺序的170->1700

    file_names = []
    # 图片命名是有规律的，所以自定义名字
    for i in range(6000):
        file = str(i)+'.jpg'
        file_names.append(os.path.join(file_path,file))
    # print(file_names)

    # 创建文件名的队列,不能随机出队
    queue = tf.train.string_input_producer(file_names, shuffle=False)

    # 文件阅读器
    reader = tf.WholeFileReader()
    key,value = reader.read(queue)

    # 图片解码
    image = tf.image.decode_jpeg(contents=value,channels=3)
    # 设置形状
    image = tf.reshape(image, [20,80,3])

    # 批量读
    image_batch = tf.train.batch([image], num_threads=1, batch_size=6000, capacity=6000)
    return image_batch


# 将验证码和标签存储到TFRecords
def write_to_tfrecords(label_batch, image_batch):
    writer = tf.python_io.TFRecordWriter('./tfrecords/captcha.tfrecords')

    # 为了时间，先写10张图片进去，过后可以遍历6000张
    # 遍历10张图片和标签，构建example协议块，存入tfrecords中

    # 将标签的数据转换为6000个tensor
    label_batch = tf.cast(label_batch,tf.uint8)


    for i in range(10):
        image = image_batch[i].tostring()
        label = label_batch[i].eval().tostring() #todo 这里将会有个问题list 不能直接转

        example = tf.train.Example(features=tf.train.Features(
            feature={
                'image':tf.train.Feature(bytes_list=tf.train.BytesList(value=[image])),
                # label是一个列表，所以页转为bytes好了
                'label':tf.train.Feature(bytes_list=tf.train.BytesList(value=[label]))
            }
        ))
        writer.write(example.SerializeToString())
    writer.close()

label_batch = read_csv()
image_batch =read_captcha()
with tf.Session() as sess:
    # 启动多线程
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess,coord=coord)


    print(label_batch.eval())
    # 转换为数字
    label_digits = transform_label_to_digist(label_batch.eval())
    # [[12,33,22,11],[10,2,3,4],[]...]

    print(image_batch.eval())

    write_to_tfrecords(label_digits,image_batch.eval())



    coord.request_stop()
    coord.join(threads=threads)





