import os

import tensorflow as tf

'''
cifar-10的数据来源：
https://www.cs.toronto.edu/~kriz/cifar.html
1、构造二进制文件队列
2、构造阅读器，读取二进制数据
3、解码数据，并分割成label和image（tf.slice）
4、图片处理
5、开启会话，启动多线程读取
6、批处理


'''
RECODE_BYTES = 3073
LABEL_BYTES = 1
IMAGE_BYTES = 3072

# 1、构造二进制文件队列
file_path = '/home/python/PycharmProjects/tensorflow_learn/day02/cifar-10-batches-bin'
files = os.listdir(file_path)
# 筛选只有bin的文件,并构建绝对路径列表
file_names = [os.path.join(file_path,file) for file in files if file[-3:] == "bin"]
print(file_names)

queue = tf.train.string_input_producer(file_names)

# 2、构造阅读器，读取二进制数据
# record_bytes一次性读取多少数据
reader = tf.FixedLengthRecordReader(record_bytes=RECODE_BYTES)
# read每次都是这样返回的
key, label_image = reader.read(queue)


# 3、解码数据，并分割成label和image（tf.slice）
label_image_raw =  tf.decode_raw(bytes=label_image,out_type=tf.uint8)
# 分割
label = tf.slice(label_image_raw,begin=[0],size=[LABEL_BYTES])
image = tf.slice(label_image_raw,begin=[LABEL_BYTES],size=[IMAGE_BYTES])
print(label)
print(image)
# Tensor("Slice:0", shape=(1,), dtype=uint8)
# Tensor("Slice_1:0", shape=(3072,), dtype=uint8)


# 4、图片处理
# 尺寸变为32*32 通道为3
image_reshape = tf.reshape(image,shape=[32,32,3])


# 6 批处理
# 标签与reshape的图片
label_batch, image_batch = tf.train.batch([label, image_reshape],batch_size=10, num_threads=1, capacity=10)

# 5、开启会话，启动多线程读取
with tf.Session() as sess:
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    # 图片值
    print(image_reshape.eval())
    # 标签值
    print(label.eval())


    # 10个标签
    print(label_batch.eval())


    coord.request_stop()
    coord.join(threads=threads)








