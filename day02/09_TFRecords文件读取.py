import os

import tensorflow as tf

'''
1、构建文件名队列
2、构造TFRecords阅读器
3、读取解析Example协议数据
4、转换格式，解码图片和标签
5、开启会话，启动多线程
6、批处理

'''
# 1、构建文件名队列
file = '/home/python/PycharmProjects/tensorflow_learn/day02/tfrecords/cifar10.tfrecords'
queue = tf.train.string_input_producer(string_tensor=[file])

# 2、构造TFRecords阅读器
reader = tf.TFRecordReader()
# 读了一个example
key, value = reader.read(queue=queue)

# 3、读取解析Example协议数据, 返回字典类型的数据
# features传键值对，读取的数据类型
features = tf.parse_single_example(serialized=value, features={
    'image': tf.FixedLenFeature(shape=[], dtype=tf.string),
    'label': tf.FixedLenFeature(shape=[], dtype=tf.int64)
})

# 4、转换格式，解码图片和标签
image_raw = features['image']
label = features['label']

# 解码
image = tf.decode_raw(image_raw, out_type=tf.uint8)
print(image)  # Tensor("DecodeRaw:0", shape=(?,), dtype=uint8)
# 发现没有形状，需要自己设置
# 宽，高，通道
image_reshape = tf.reshape(image, shape=[32, 32, 3])
# label定义形状
label = tf.reshape(label, shape=[1])

# 6、批处理 需传入形状确定的tensor
label_batch, image_batch = tf.train.batch([label, image_reshape], batch_size=10, num_threads=3, capacity=10)

# 5、开启会话，启动多线程
with tf.Session() as sess:
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    # 查看值
    print(image_raw.eval())
    print(label.eval())
    print(image.eval())
    print(image_reshape.eval())

    # 查看批处理的结果
    print(label_batch.eval())

    coord.request_stop()
    coord.join(threads=threads)
