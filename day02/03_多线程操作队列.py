import tensorflow as tf

'''
通过队列管理器来实现变量加1，入队，主线程出队列的操作，
观察效果？(异步操作)

'''
queue = tf.FIFOQueue(capacity=100, dtypes=tf.int32)

# 实现变量加1，入队
var = tf.Variable(initial_value=0, dtype=tf.int32)
var_add = var.assign_add(delta=1)
enqueue_op = queue.enqueue(var_add)


# 创建队列管理器
#                                                                       需要几个线程
queue_runner = tf.train.QueueRunner(queue=queue, enqueue_ops=[enqueue_op]*1)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)

    #todo 创建线程协调器,防止报错
    coord = tf.train.Coordinator()

    # 创建线程，start为启动，这些线程就会开始执行入队操作
    # 返回所有线程
    threads = queue_runner.create_threads(sess=sess, coord=coord, start=True)

    # 队列数据的入列操作由queue_runner的线程去完成，只需要在主线程出列操作
    # print(queue.dequeue().eval())

    for i in range(100):
        # 看队列的大小是否在变化
        print(queue.size().eval())

    # 请求线程的终止
    coord.request_stop()
    # 等待线程终止
    coord.join(threads=threads)


