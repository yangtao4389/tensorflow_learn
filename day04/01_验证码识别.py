import tensorflow as tf

'''
1、验证码数据准备,存储为TFRecords文件
2、构建文件名队列
3、创建阅读器读取数据
4、解析example协议处并处理数据
5、批处理
7、构建两层神经全连接神经网络
8、交叉熵和反向传播（梯度下降）
9、创建会话，开启多线程，进行训练
10、准确率计算

'''
# 2、构建文件名队列
path = '/home/python/PycharmProjects/tensorflow_learn/day04/tfrecords/captcha.tfrecords'
queue = tf.train.string_input_producer(string_tensor=[path])

# 3、创建阅读器读取数据
reader = tf.TFRecordReader()
# key文件名，value读取为一个example协议块
key, value = reader.read(queue=queue)

features = tf.parse_single_example(serialized=value,features={
    'image':tf.FixedLenFeature(shape=[], dtype=tf.string),
    'label':tf.FixedLenFeature(shape=[], dtype=tf.string)
})

image_raw = features['image']
label_raw = features['label']


# 解码二进制字符串
image = tf.decode_raw(bytes=image_raw, out_type=tf.uint8)
label = tf.decode_raw(bytes=label_raw,out_type=tf.uint8)


# 批处理 要对需要批处理的tensor设置形状
image_reshape = tf.reshape(image,shape=[20,80,3])
label_reshape = tf.reshape(label, shape=[4])
image_batch,label_batch = tf.train.batch([image_reshape,label_reshape], batch_size=50, num_threads=1, capacity=50)

# 5 构建神经网络
x = tf.placeholder(dtype=tf.float32)
y = tf.placeholder(dtype=tf.int32)

# 展开x,因为batch过后为一个整体,-1不确定传过来多少批次，如果是50则自动为50
x_image = tf.reshape(x, shape=[-1,20*80*3])

# 原始输出层
weight = tf.Variable(tf.random_normal(shape=[20*80*3,4*26]))
bias = tf.Variable(tf.random_normal(shape=[4*26]))

logits = tf.matmul(x_image,weight) + bias

# softmax回归与交叉熵损失
# 将y变成one-hot编码
y_one_hot = tf.one_hot(indices=y,depth=26)
# [[[0. 0. 0. ... 0. 0. 0.]
#   [0. 0. 0. ... 0. 0. 0.]
#   [0. 0. 0. ... 0. 0. 0.]
#   [1. 0. 0. ... 0. 0. 0.]]

# 需要转换形状，去掉中间的括号,应该说是少一层
y_one_hot_reshape = tf.reshape(y_one_hot, shape=[-1,4*26])
# [[0. 0. 0. ... 0. 0. 0.]
#  [0. 0. 0. ... 0. 0. 0.]
#  [0. 0. 0. ... 0. 0. 0.]
#  ...
#  [0. 0. 1. ... 0. 1. 0.]
#  [0. 0. 0. ... 0. 0. 0.]
#  [0. 0. 0. ... 0. 0. 0.]]

loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_one_hot_reshape,logits=logits))

train_op = tf.train.GradientDescentOptimizer(learning_rate=0.01).minimize(loss)


# 10 准确率的计算
# y_ont_hot = [num,4,26] axis为2维
# logits = [50,4*26]->[50,4,26] 才可以比较
logits_reshape = tf.reshape(logits, shape=[-1,4,26])
equal_list = tf.equal(tf.argmax(y_one_hot,axis=2),tf.argmax(logits_reshape,axis=2))
# [[True,True,True,False],
# [True,True,False,True]]
# 变为1 0
# 单个字母的预测的准确率
accuracy = tf.reduce_mean(tf.cast(equal_list,tf.float32))
# 也可以自己转换为4个字母的




init = tf.global_variables_initializer()
with tf.Session() as sess:
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess,coord=coord)

    sess.run(init)
    # 查看读出里的数据
    # print(image_raw.eval()) #二进制
    # print(label_raw.eval()) #二进制

    # print(image.eval()) # 数字[251 252 247 ... 254 255 255]
    # print(label.eval()) # 数字[22 10  7 10]

    # print(image_batch.eval())
    # print(label_batch.eval()) #  [ 7 10 17 25]

    for i in range(1000):
        images,labels =sess.run([image_batch,label_batch])
        sess.run(train_op,feed_dict={x:images,y:labels})

        # print(sess.run(y_one_hot,feed_dict={x:images,y:labels}))
        # print(sess.run(y_one_hot_reshape,feed_dict={x:images,y:labels}))
        print(sess.run(accuracy,feed_dict={x:images,y:labels}))


    coord.request_stop()
    coord.join()


