import tensorflow as tf

# 模型参数
W = tf.Variable([.3], dtype=tf.float32)
b = tf.Variable([-.3], dtype=tf.float32)
# 输入与输出
x = tf.placeholder(tf.float32)
linear_model = W * x + b
y = tf.placeholder(tf.float32)

# loss函数
loss = tf.reduce_sum(tf.square(linear_model-y))

# 推荐
optimizer = tf.train.GradientDescentOptimizer(0.01)
train = optimizer.minimize(loss)

# 训练数据
x_train = [1,2,3,4]
# 目标值
y_train = [0,-1,-2,-3]

# 循环训练
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)
for i in range(1000):
    sess.run(train,feed_dict={x:x_train,y:y_train})

# 估计当前训练值
curr_W,curr_b,curr_loss = sess.run([W,b,loss],feed_dict={x:x_train,y:y_train})
print("W:%s,b:%s,loss:%s" % (curr_W,curr_b,curr_loss))
# W:[-0.9999969],b:[0.9999908],loss:5.6999738e-11




