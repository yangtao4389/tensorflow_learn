import tensorflow as tf

# 定义两个权重
# 权重为2行3列的值
weight1 = tf.Variable(tf.random_normal(shape=[2,3]))

weight2 = tf.Variable(tf.random_normal(shape=[3,1]))


# 定义输入x
x = tf.constant([1,1],shape=[1,2],dtype=tf.float32)

# 求y
# 先求a
# 1,2 * 2,3   -> 1,3
a = tf.matmul(x,weight1)
# 1,3 * 3,1  -> 1,1
y = tf.matmul(a,weight2)

init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)

    print(sess.run(y))
    # [[-0.19288845]]



