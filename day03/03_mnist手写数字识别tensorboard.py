import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

'''
1、准备数据(input_data.read_data_sets)
2、输入层
3、隐藏层
4、激活函数
5、原始输出层
6、Softmax回归和交叉熵
7、梯度下降，开启会话，训练模型
8、模型评估（计算准确性）
9、Tensorboard可视化

'''
# 1、准备数据(input_data.read_data_sets)
# 要使得到的目标值为one-hot编码
path = '/home/python/PycharmProjects/tensorflow_learn/day03/mnist/input_data'
data = input_data.read_data_sets(train_dir=path, one_hot=True)

# 2 输入层
x = tf.placeholder(dtype=tf.float32,name='x')
y = tf.placeholder(dtype=tf.int32,name='y')

# 3 隐藏层
# 做一个矩阵的运算，x * w1 + bias（偏置）
# 形状为784*500 ，500是我们自定义的隐藏层
INPUT_NODE = 784
HIDE_LAYER_NODE = 500
OUTPUT_NODE = 10
weight1 = tf.Variable(tf.random_normal(shape=[INPUT_NODE,HIDE_LAYER_NODE]),name='weight1')
bias1 = tf.Variable(tf.random_normal(shape=[HIDE_LAYER_NODE]),name='bias1')

hide_layer_out = tf.matmul(x,weight1) + bias1

# 4 激活函数
relu_out = tf.nn.relu(hide_layer_out)

#5 原始输出层
weight2 = tf.Variable(tf.random_normal(shape=[HIDE_LAYER_NODE,OUTPUT_NODE]),name='weight2')
bias2 = tf.Variable(tf.random_normal(shape=[OUTPUT_NODE]),name='bias2')
logits = tf.matmul(relu_out,weight2) + bias2

#6 softmax回归和交叉熵
# labels是真实结果，logits是原始结果
# 为了防止一次传入多个值，所以用平均值来作为loss函数
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=logits),name='loss')

# 7 梯度下降，开启会话，训练模型
# learning_rate 学习率，并是loss最小
train_op = tf.train.GradientDescentOptimizer(learning_rate=0.3).minimize(loss)


# 模型评估（计算准确性
# y真实值 [50,10] 50行(样本为50)，10列
# [0000011110]
# logits预测值 [50,10] 对应50个结果
# [0.1,0.1..]
# equal_list50个结果【true,false...】
equal_list = tf.equal(tf.argmax(y,axis=1),tf.argmax(logits,axis=1))
# 类型强转cast 【1,0.】
accuracy = tf.reduce_mean(tf.cast(equal_list,dtype=tf.float32))


init = tf.global_variables_initializer()

# 9 tensorboard可视化
file_writer = tf.summary.FileWriter(logdir='./log',graph=tf.get_default_graph())

# 记录变量
tf.summary.scalar(name='loss',tensor=loss)
tf.summary.scalar(name='accuracy',tensor=accuracy)
tf.summary.histogram(name='weight1',values=weight1)
tf.summary.histogram(name='weight2',values=weight2)

# 合并
merge_all = tf.summary.merge_all()

with tf.Session() as sess:
    sess.run(init)
    # 得到50张图片，50个标签，都是onehot编码
    image_batch,label_batch = data.train.next_batch(batch_size=50)
    for i in range(100):
        # 训练多次
        sess.run(train_op, feed_dict={x:image_batch,y:label_batch})
        print(sess.run(accuracy,feed_dict={x:image_batch,y:label_batch}))

        # 执行合并操作
        summary = sess.run(merge_all,feed_dict={x:image_batch,y:label_batch})
        file_writer.add_summary(summary,i)








