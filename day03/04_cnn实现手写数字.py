import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data


'''

1、准备数据
2、输入层                 [28,28,1]
3、卷积层1（激活relu）
	filter1=[5,5,1,32],strides=[1,1,1,1],padding=‘SAME’输出：[28,28,32]
4、池化层1
	kize=[1,2,1,1],strides=[1,2,2,1],padding=‘SAME’ 输出：[14,14,32]
5、卷积层2（激活relu）
	Filter2=[5,5,32,64],strides=[1,1,1,1],padding=‘SAME’ 输出:[14,14,64]
7、池化层2
	kize=[1,2,2,1],strides=[1,2,2,1],padding=‘SAME’, 输出：[7,7,64]
8、全连接层1（激活）
	假设1024个全连接层
	输入：[7*7*64,1024]
9、全连接层2（激活）
10、原始输出层
	[1024,10]
11、Softmax回归
12、梯度下降，开启会话，训练模型
13、准确率的计算

'''
path = '/home/python/PycharmProjects/tensorflow_learn/day03/mnist/input_data'
data = input_data.read_data_sets(train_dir=path, one_hot=True)


# 2输入层
x = tf.placeholder(dtype=tf.float32)
y = tf.placeholder(dtype=tf.int32)

# 3 卷积层
x_image = tf.reshape(x,shape=[-1,28,28,1])
filter1 = tf.Variable(tf.random_normal(shape=[5,5,1,32]))
conv1 = tf.nn.conv2d(x_image,filter=filter1,strides=[1,1,1,1],padding='SAME')
# 每个通道一个偏置
bias = tf.Variable(tf.random_normal(shape=[32]))
# 激活
relu1 = tf.nn.relu(conv1+bias)

#4池化层
pool1 = tf.nn.max_pool(relu1,ksize=[1,2,2,1], strides=[1,2,2,1],padding='SAME')
# [14,14,32]

# 5 卷积层2
filter2 = tf.Variable(tf.random_normal(shape=[5,5,32,64]))
conv2 = tf.nn.conv2d(pool1, filter2,strides=[1,1,1,1], padding='SAME')
bias2 = tf.Variable(tf.random_normal(shape=[64]))
relu2 = tf.nn.relu(conv2+bias2)

# 池化层
pool2 = tf.nn.max_pool(relu2, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')
# [7,7,64]

# 全连接层
# 需要做一个形状变化
fc_input = tf.reshape(pool2, shape=[-1,7*7*64])
weight = tf.Variable(tf.random_normal(shape=[7*7*64,1024]))
fc_bias1 = tf.Variable(tf.random_normal(shape=[1024]))

# 激活后输出
fc_out = tf.nn.relu(tf.matmul(fc_input,weight) + fc_bias1)

# 原始输出层
# 10个
weight2 = tf.Variable(tf.random_normal([1024,10]))
fc_bias2 = tf.Variable(tf.random_normal(shape=[10]))
logits = tf.matmul(fc_out,weight2) + fc_bias2

# softmax回归
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=logits))

# 梯度下降的训练
train_op = tf.train.GradientDescentOptimizer(learning_rate=0.0001).minimize(loss)

# 计算准确率
# accuracy = tf.reduce_mean(tf.cast(eq))
equal_list = tf.equal(tf.argmax(y,axis=1),tf.argmax(logits,axis=1))
# 类型强转cast 【1,0.】
accuracy = tf.reduce_mean(tf.cast(equal_list,dtype=tf.float32))

init = tf.global_variables_initializer()
# 开启会话， 实现训练
with tf.Session() as sess:
    sess.run(init)
    for i in range(100):
        image_batch, label_batch = data.train.next_batch(batch_size=50)

        sess.run(train_op,feed_dict={x:image_batch, y:label_batch})

        print(sess.run(accuracy,feed_dict={x:image_batch, y:label_batch}))









