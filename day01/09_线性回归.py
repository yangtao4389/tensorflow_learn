import tensorflow as tf

# 定义输入占位符
from tensorflow.python.training.gradient_descent import GradientDescentOptimizer

# 定义输入模块
with tf.variable_scope("input"):
    x = tf.placeholder(dtype=tf.float32,name='x')
    y = tf.placeholder(dtype=tf.float32,name='y')

# 定义模型模块
with tf.variable_scope('model'):
    # 定义参数，不断更新
    weight = tf.Variable(initial_value=3.0,name='weight')
    bias = tf.Variable(initial_value=3.0,name='bias')

    # 构建模型 y = w*x + b
    linear_model = tf.multiply(weight,x) + bias
    # 等同于
    # tf.add(tf.multiply(weight,x),bias)

# 损失模块
with tf.variable_scope('loss'):
    # 找损失函数
    # 将所有的损失平方加起来
    loss = tf.reduce_sum(tf.square(linear_model -y))

# 训练模块
with tf.variable_scope('train'):
    # 调整参数，使得损失最少
    # weight_update = weight.assign(1)
    # bias_update = bias.assign(1)
    # 通过机器学习来实现线性回归，自动更新参数
    train_op = GradientDescentOptimizer(learning_rate=0.01).minimize(loss)


# 变量初始化
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    # 执行assgign更新参数
    # sess.run([weight_update,bias_update])

    # print(sess.run(loss, feed_dict={x:[0,1,2,3],y:[1,2,3,4]}))
    # 当为0时，模型y= 1*x +1

    for i in range(100):
        sess.run(train_op,feed_dict={x:[0,1,8,3],y:[1,2,3,4]}) # 执行训练操作
        # 每次训练之后会自动更新参数weight bias
        # print(weight,bias)
        print(sess.run([weight,bias]))

    tf.summary.FileWriter(logdir='./log', graph=tf.get_default_graph())




