import tensorflow as tf
import os

# 定义输入占位符
from tensorflow.python.training.gradient_descent import GradientDescentOptimizer




# 定义输入模块
with tf.variable_scope("input"):
    x = tf.placeholder(dtype=tf.float32,name='x')
    y = tf.placeholder(dtype=tf.float32,name='y')

# 定义模型模块
with tf.variable_scope('model'):
    # 定义参数，不断更新
    weight = tf.Variable(initial_value=3.0,name='weight')
    bias = tf.Variable(initial_value=3.0,name='bias')

    # 构建模型 y = w*x + b
    linear_model = tf.multiply(weight,x) + bias
    # 等同于
    # tf.add(tf.multiply(weight,x),bias)

# 损失模块
with tf.variable_scope('loss'):
    # 找损失函数
    # 将所有的损失平方加起来
    loss = tf.reduce_sum(tf.square(linear_model -y))

# 训练模块
with tf.variable_scope('train'):
    # 调整参数，使得损失最少
    # weight_update = weight.assign(1)
    # bias_update = bias.assign(1)
    # 通过机器学习来实现线性回归，自动更新参数
    train_op = GradientDescentOptimizer(learning_rate=0.01).minimize(loss)


file_writer = tf.summary.FileWriter(logdir='./log/', graph=tf.get_default_graph())
tf.summary.scalar(tensor=weight,name='weight')
tf.summary.scalar(tensor=bias,name='bias')
tf.summary.scalar(tensor=loss,name='loss')
# 定义合并操作
merge_all = tf.summary.merge_all()


# 变量初始化
init = tf.global_variables_initializer()


# 定义saver保存模型
saver = tf.train.Saver(var_list=[weight,bias],max_to_keep=3)

with tf.Session() as sess:
    sess.run(init)
    # 执行assgign更新参数
    # sess.run([weight_update,bias_update])

    # print(sess.run(loss, feed_dict={x:[0,1,2,3],y:[1,2,3,4]}))
    # 当为0时，模型y= 1*x +1


    #todo 再次训练之前检查是否有checkpoint文件，
    #todo 如果有，加载里面保存的参数到会话
    #todo 可以多次刷新来查看是否是这样的
    if os.path.exists('./save/checkpoint'):
        saver.restore(sess=sess,save_path='./save/')

    for i in range(10):
        sess.run(train_op,feed_dict={x:[0,1,2,3],y:[1,2,3,4]}) # 执行训练操作
        # 每次训练之后会自动更新参数weight bias
        # print(weight,bias)
        print(sess.run([weight,bias]))
        # 每次迭代执行合并操作
        summary = sess.run(merge_all, feed_dict={x: [0, 1, 2, 3], y: [1, 2, 3, 4]})
        # 将summary添加到
        file_writer.add_summary(summary,i)


    saver.save(sess=sess, save_path='./save/')

if __name__ == '__main__':
    pass
    # 未实现
