import tensorflow as tf

# 常量 不能直接改变
a = tf.constant(3)

# 变量
var = tf.Variable(initial_value=3, name=None)
print(var)
# 全局变量的初始化器
init = tf.global_variables_initializer()

# 改变var的值
var1 = var.assign(4)

# 给原来变量增加4
var1 = var.assign_add(delta=4)
# 给原来变量减4
var1 = var.assign_sub(delta=4)

with tf.Session() as sess:
    # 必须初始化才可以用，执行初始化器
    sess.run(init)
    print('初始化的var值',sess.run(var))
    # 执行assign的操作
    print('var1为更新后的值',sess.run(var1))
    print('更新后的var值',sess.run(var))  # 发现var值也更新了

