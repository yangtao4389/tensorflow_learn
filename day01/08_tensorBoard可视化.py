import tensorflow as tf
a = tf.constant(3, name='a')
b = tf.constant(4, name='b')
result = tf.add(a,b)

with tf.Session() as sess:



    # 创建FileWriter，序列化events文件
    writer = tf.summary.FileWriter(logdir='./log',graph=tf.get_default_graph())

    sess.run(result)


writer.close()

