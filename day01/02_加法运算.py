
# python中加法
'''
def add(a,b):
    return a+b

a = 3
b = 4
print(add(a,b))
'''

import tensorflow as tf
a = tf.constant(3)
print("a",a)  #Tensor("Const:0", shape=(), dtype=int32)
# 创建的是Tensor

b = tf.constant(4)
result = tf.add(a,b)
print(result)
# Tensor("Add:0", shape=(), dtype=int32)
# result发现是一个Tensor

# 创建会话，执行Tensor
sess = tf.Session()
print(sess.run(result))
sess.close()




