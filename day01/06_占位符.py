import tensorflow as tf

# 不清楚ab具体的值，只知道类型，先使用占位符
# 构建计算图
a = tf.placeholder(dtype=tf.int32)
b = tf.placeholder(dtype=tf.int32)
result = tf.add(a, b)

# 创建会话，执行Tensor
sess = tf.Session()
# todo feed_dict给占位符喂数据
print(sess.run(result, feed_dict={a:3, b:4}))
# 等价run
print(result.eval(session=sess,feed_dict={a:3,b:4}))


# 由于占位符没有指定形状，后面可以更改它的形状
a.set_shape([3,2])
print(a.get_shape())
# 设置好后就不能改了。会报错
a.set_shape([3,5])
