import tensorflow as tf

# 获取计算图
default_graph = tf.get_default_graph()
print(default_graph) # graph对象


a = tf.constant(3)
b = tf.constant(4)
result = tf.add(a,b)

print("a的计算图",a.graph)
print("b的计算图",b.graph)
print("result的计算图",result.graph)

# 创建会话，执行Tensor
sess = tf.Session()
print("sess的计算图",sess.graph)

print(sess.run(result))
sess.close()

# 自己创建计算图
graph = tf.Graph()
print("新建的graph：",graph)
with graph.as_default():
    print("获取到当前的graph",tf.get_default_graph())

# 不同的计算图之间不能共用张量或者运算
# 所以刚刚在默认计算图中所有的tenser用的graph都是同一个
# 当自己新建过后，也需要通过with来实现




