import tensorflow as tf

a = tf.constant(3)  # 0阶张量 标量

b = tf.constant([1, 2, 3])  # 1阶张量 1维数组

c = tf.constant([[1, 2], [3, 4]])  # 2阶张量 2维数组

# 上述abc都是张量，可以简单理解为多维数组


#todo 为什么说tensor保存的是计算过程，而不是数字
a = tf.constant(3)
print("a",a)
#Tensor("Const:0", shape=(), dtype=int32)
# 创建的是Tensor

b = tf.constant(4)
result = tf.add(a,b)
print(result)
# Tensor("Add:0", shape=(), dtype=int32)
# result发现是一个Tensor
#todo 查看操作
print(result.op)


# 创建会话，执行Tensor，拿到结果
sess = tf.Session()
print(sess.run(result))
sess.close()



# 全为0或者全为1张量的创建
zeros = tf.zeros(shape=[3,2],dtype=tf.int32,name="zeros")
print(zeros) #Tensor("zeros:0", shape=(3, 2), dtype=int32)
sess = tf.Session()
print(sess.run(zeros))
# [[0 0]
#  [0 0]
#  [0 0]]
sess.close()


# 创建正态分布
#todo 一般用这种方法初始化变量取值
rn = tf.random_normal(shape=[3,3])
sess = tf.Session()
print(sess.run(rn))
# [[-0.829186    0.2352067  -1.3037206 ]
#  [-0.86472535  1.1060531   0.5275917 ]
#  [ 0.44795138  1.5641197  -0.09979464]]
sess.close()

# 张量的形状
rn = tf.random_normal(shape=[3,3])
print(rn.get_shape()) #拿到（3,3）

# rn.set_shape([1,9]) # 设置shape会报错
# 如果张量的形状已经确定，就不可更改

new_rn = tf.reshape(tensor=rn, shape=[1,9]) # 返回新的张量
print(new_rn) # 名字变为了reshape




# 张量的数据类型的改变
string_tensor = tf.constant("23")
n = tf.string_to_number(string_tensor=string_tensor, out_type=tf.int32)
print(n)
# 可以定义，
sess = tf.Session()
print(sess.run(n))
print(n)
sess.close()



# 张量的运算
# a = tf.constant(2,dtype=tf.float32)
# result = tf.square(a)  # 对a平方运算
# result = tf.log(a)  # 对a取对数运算

# 矩阵运算
# m*n   n*k = m*k
# a = tf.random_normal(shape=[3,2])
# b = tf.random_normal(shape=[2,5])
# result = tf.matmul(a,b)


# 降维运算
a = tf.constant([[1,1,1],[2,2,2]])
result = tf.reduce_sum(a) # 求和 9
result = tf.reduce_sum(a, axis=0) #[3,3,3]
result = tf.reduce_sum(a, axis=1) #[3,6]


result = tf.reduce_mean(a) # 平均值 1
result = tf.reduce_mean(a,axis=1) # 平均值 [1,2]


# 返回最大值的下标
result = tf.argmax(a,axis=0) # [1,1,1]



sess = tf.Session()
print(sess.run(result))
sess.close()









