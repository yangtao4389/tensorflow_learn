import tensorflow as tf

# 获取计算图
default_graph = tf.get_default_graph()
print(default_graph)  # graph对象

a = tf.constant(3)
b = tf.constant(4)
result = tf.add(a, b)

# 创建会话，执行Tensor
sess = tf.Session()
# todo 会话的两种运行方式
# print(sess.run(result))
# 等价run
print(result.eval(session=sess))

# 关闭会话，因为会话管理着计算的资源，cpu，gpu，所以需要释放资源
sess.close()

# 当没执行完的时候，session就没关闭，所以用上下文
with tf.Session() as sess:
    print(sess.run(result))
    print(result.eval())  # 不用指定session了

# 获取默认会话
session = tf.get_default_session()
print(session)  # none  所以没有默认的会话

# 创建默认的会话
sess = tf.Session()
with sess.as_default():
    print(result.eval())

# 如果使用了GPU版本，在GPU不可用的情况下，自动切换到cpu
# allow_soft_placement
config = tf.ConfigProto(allow_soft_placement=True,
                        log_device_placement=True)
# log_device_placement可以看到这些是运行在哪些设备

sess = tf.Session(config=config)
with sess.as_default():
    print(result.eval())

